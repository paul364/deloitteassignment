//
//  FlickrPhoto.swift
//  Gallery
//
//  Created by Somebody Someone on 30/09/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct FlickrPhoto: Decodable, Image {
    let id: String?
    let owner: String?
    let secret: String?
    let server: String?
    let title: String?
    let farm: Int?
    let ispublic: Int?
    let isfriend: Int?
    let isfamily: Int?
    let url_m: String?
    let height_m: Int?
    let width_m: Int?
    
    var url: URL? {
        if let url = url_m {
            return URL(string: url)
        } else {
            return nil
        }
    }
}
