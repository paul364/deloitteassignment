//
//  Image.swift
//  Gallery
//
//  Created by Somebody Someone on 30/09/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

// Protocol for the image that we expect from the image source. Contains data needed to display it
protocol Image {
    var url: URL? { get }
}
