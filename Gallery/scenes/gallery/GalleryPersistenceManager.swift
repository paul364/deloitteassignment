//
//  GalleryPersistenceManager.swift
//  Gallery
//
//  Created by Somebody Someone on 30/09/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

protocol GalleryPersistenceManager: class {
    var searchQueries: [String] { get set }
}

extension UserDefaults: GalleryPersistenceManager {
    var searchQueries: [String] {
        get {
            return object(forKey: #function) as? [String] ?? []
        }
        set {
            set(newValue, forKey: #function)
        }
    }
}
