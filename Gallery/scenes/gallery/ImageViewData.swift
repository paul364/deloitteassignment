//
//  ImageViewData.swift
//  Gallery
//
//  Created by Somebody Someone on 30/09/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct ImageViewData {
    let url: URL?
    
    init(url: URL?) {
        self.url = url
    }
}

extension ImageViewData: Equatable {}
