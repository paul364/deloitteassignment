//
//  GalleryUITests.swift
//  GalleryUITests
//
//  Created by Somebody Someone on 30/09/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import XCTest

class GalleryUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchPopoverBehavior() {
        let app = XCUIApplication()
        let searchBar = app.tables.searchFields["Search"]
        XCTAssert(!searchBar.exists)
        app.navigationBars["Flickr"].buttons["Search"].tap()
        XCTAssert(searchBar.exists && searchBar.isEnabled)
        let popoverDismissRegion = app.otherElements["PopoverDismissRegion"]
        popoverDismissRegion.tap()
        XCTAssert(!popoverDismissRegion.exists)
    }
}
